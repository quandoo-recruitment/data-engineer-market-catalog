# Data Engineer Market Catalog


## Overview


This step of the interview process will allow us to better understand how do you work and what natural priorioties do you have. It is not about evaluating how good you are, it is rather about understanding whether you and our team will be a good fit. Completing the task of the assignment should not take you more than 4 hours. Please submit your response in whichever format you deem appropriate.

The task is a user story that will require you to treat it as a JIRA ticket for the team.


## Tasks

**User story**

As a Restaurant Owner I would like to get the most accurate weather forecast for Berlin for tomorrow, so I could prepare my tables accordingly. To do that I would like to use data from multiple weather services and merge it in a format that Quandoo can consume and output in my Dashboard.

**AC:**

- Input data is consumed from Public API of https://weatherstack.com/ and https://openweathermap.org/ 
- The output data should not have duplicate fields and has everything useful for a Restaurant Owner
